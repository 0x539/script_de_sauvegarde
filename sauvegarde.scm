#!/usr/bin/guile \
-e début -s
!#

;; Auteur : Corentin Bocquillon <corentin@nybble.fr> 2017

;; Ce logiciel est régi par la licence CeCILL soumise au droit français et
;; respectant les principes de diffusion des logiciels libres. Vous pouvez
;; utiliser, modifier et/ou redistribuer ce programme sous les conditions
;; de la licence CeCILL telle que diffusée par le CEA, le CNRS et l’INRIA
;; sur le site « http://www.cecill.info ».

;; En contrepartie de l’accessibilité au code source et des droits de copie,
;; de modification et de redistribution accordés par cette licence, il n’est
;; offert aux utilisateurs qu’une garantie limitée.  Pour les mêmes raisons,
;; seule une responsabilité restreinte pèse sur l’auteur du programme,  le
;; titulaire des droits patrimoniaux et les concédants successifs.

;; A cet égard  l’attention de l’utilisateur est attirée sur les risques
;; associés au chargement,  à l’utilisation,  à la modification et/ou au
;; développement et à la reproduction du logiciel par l’utilisateur étant
;; donné sa spécificité de logiciel libre, qui peut le rendre complexe à
;; manipuler et qui le réserve donc à des développeurs et des professionnels
;; avertis possédant  des  connaissances  informatiques approfondies.  Les
;; utilisateurs sont donc invités à charger  et  tester  l’adéquation  du
;; logiciel à leurs besoins dans des conditions permettant d’assurer la
;; sécurité de leurs systèmes et ou de leurs données et, plus généralement,
;; à l’utiliser et l’exploiter dans les mêmes conditions de sécurité.

;; Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
;; pris connaissance de la licence CeCILL, et que vous en avez accepté les
;; termes.

(use-modules (ice-9 ftw)
             (ice-9 eval-string)
             (ice-9 match)
             (ice-9 popen)
             (ice-9 rdelim))

(load "variables-configuration.scm")
(define répertoire-de-sauvegarde-actuelle "")

(define (début args)
  (if (null? (cdr args))
      (load "configuration.scm")
      (if (string? (cadr args))
          (load (cadr args))))
  (set! répertoire-de-sauvegarde (string-append
                                  (dirname répertoire-de-sauvegarde)
                                  "/" (basename répertoire-de-sauvegarde)))
  (lancer-sauvegarde))

(define (afficher-ligne chaîne)
  (display chaîne)
  (newline))

(define (lancer-sauvegarde)
  (if (not (null? fichiers-à-sauvegarder))
      (begin
        (set! fichiers-à-sauvegarder (filtrer-fichiers fichiers-à-sauvegarder))
        (set! répertoire-de-sauvegarde-actuelle
              (string-append répertoire-de-sauvegarde "/"
                             (strftime "%FT%T%z" (localtime (current-time)))))
        (créer-répertoire-si-inexistant répertoire-de-sauvegarde)
        (let ((répertoire-actuelle (getcwd))
              (répertoire-dernière-sauvegarde
               (obtenir-dossier-dernière-sauvegarde répertoire-de-sauvegarde)))
          (créer-répertoire-si-inexistant répertoire-de-sauvegarde-actuelle)
          (chdir répertoire-de-sauvegarde-actuelle)
          (for-each (lambda (fichier)
                      (sauvegarder fichier répertoire-dernière-sauvegarde))
                    (filtrer-fichiers fichiers-à-sauvegarder))
          (chdir répertoire-actuelle)))
      (afficher-ligne "Il n’y a pas de fichier à sauvegarder.")))

(define (filtrer-fichiers fichiers)
  (if (null? fichiers) fichiers
      (catch #t
        (lambda ()
          (let ((s (stat (car fichiers))))
            (if (or (and (eq? (stat:type s) 'regular)
                         (access? (car fichiers) R_OK))
                    (and (eq? (stat:type s) 'directory)
                         (access? (car fichiers) X_OK)
                         (not (string=? (car fichiers)
                                        (string-append répertoire-de-sauvegarde
                                                       "/")))))
                (cons (car fichiers) (filtrer-fichiers (cdr fichiers)))
                (filtrer-fichiers (cdr fichiers)))))
        (lambda (clef . e)
          (filtrer-fichiers (cdr fichiers))))))

(define (créer-répertoire-si-inexistant chemin)
  (catch #t (lambda ()
              (mkdir chemin))
    (lambda (clef . e) #nil)))

(define (créer-répertoires chemin)
  (let créer-répertoire ((nom-répertoires (string-split (dirname chemin) #\/)))
    (if (not (null? nom-répertoires))
        (begin
          (if (not (string-null? (car nom-répertoires)))
              (begin (créer-répertoire-si-inexistant (car nom-répertoires))
                     (chdir (car nom-répertoires))))
          (créer-répertoire (cdr nom-répertoires))))))

(define (sauvegarder chemin répertoire-dernière-sauvegarde)
  (créer-répertoires chemin)
  (let ((s (stat chemin)))
    (cond ((eq? (stat:type s) 'regular)
           (copier-fichier-ici chemin))
          ((eq? (stat:type s) 'directory)
           (copier-arbre-ici (file-system-tree chemin)
                             (string-append (dirname chemin) "/")
                             répertoire-dernière-sauvegarde))))
  (chdir répertoire-de-sauvegarde-actuelle))

(define (copier-fichier-ici chemin répertoire-dernière-sauvegarde)
  ;; Il faut comparer le fichier avec celui de la dernière sauvegarde.
  ;; S’ils ont le même hash, on crée un lien.
  ;; Sinon on fait une copie.
  (let* ((chemin-fichier-sauvegardé (string-append répertoire-dernière-sauvegarde chemin))
         (hash-sauvegarde (sha256 chemin-fichier-sauvegardé))
         (hash-fichier (sha256 chemin)))
    (if (or (not hash-sauvegarde)
            (not (string=? hash-sauvegarde hash-fichier)))
        (copy-file chemin (basename chemin))
        (link chemin-fichier-sauvegardé (basename chemin)))))

(define (copier-arbre-ici arbre préfixe-chemin répertoire-dernière-sauvegarde)
  (let ((dossier-départ (getcwd)))
    (cond ((null? arbre) #nil)
          ((list? (car arbre))
           (copier-arbre-ici (car arbre) préfixe-chemin
                             répertoire-dernière-sauvegarde)
           (copier-arbre-ici (cdr arbre) préfixe-chemin
                             répertoire-dernière-sauvegarde))
          ((string=? (string-append préfixe-chemin (car arbre))
                     répertoire-de-sauvegarde) #nil)
          ((eq? (stat:type (cadr arbre)) 'directory)
           (mkdir (car arbre))
           (chdir (car arbre))
           (copier-arbre-ici (cddr arbre)
                             (string-append préfixe-chemin
                                            (car arbre) "/")
                             répertoire-dernière-sauvegarde)
           (chdir dossier-départ))
          ((eq? (stat:type (cadr arbre)) 'regular)
           (copier-fichier-ici (string-append préfixe-chemin (car arbre))
                               répertoire-dernière-sauvegarde)))))

(define (obtenir-dossier-dernière-sauvegarde répertoire-de-sauvegarde)
  (let* ((fichiers-et-stats (cddr
                   (file-system-tree
                    répertoire-de-sauvegarde
                    (lambda (nom stat)
                      (if (string=? nom répertoire-de-sauvegarde) #t #f)))))
         (dates-iso (map car fichiers-et-stats))
         (dates-tm (map (lambda (e) (car (strptime "%FT%T%z" e))) dates-iso))
         (dates-epoch-triées (sort (map (lambda (e) (car (mktime e)))
                                        dates-tm) >))
         (dernière-date-iso (false-if-exception (strftime "%FT%T%z"
                                                (localtime
                                                 (car dates-epoch-triées)))))
         (chemin-dernière-sauvegarde
          (if (string? dernière-date-iso)
              (string-append répertoire-de-sauvegarde "/" dernière-date-iso)
              #f)))
    chemin-dernière-sauvegarde))

(define (fichier-existant? chemin)
  (if (not (false-if-exception (stat chemin)))
      #f
      #t))

(define (sha256 chemin)
  (if (not (fichier-existant? chemin))
      #f
      (let* ((retour (open-input-pipe (string-append "sha256sum \"" chemin "\"")))
             (chaîne-retour (read-line retour))
             (hash (car (string-split chaîne-retour #\ ))))
        hash)))
